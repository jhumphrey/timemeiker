minetest.register_node("timemeiker:hourglass", {
	description = "hourglass",
	tiles = {"hglass_top.png","hglass_top.png","hglass_side.png","hglass_side.png","hglass_side.png","hglass_side.png" },
	groups = {cracky=3},
	on_punch= function(self, puncher, time_from_last_punch, tool_capabilities, dir, damage)
		t = minetest.get_timeofday()
		t = t + 0.07
		if t > 1  then
			t = 0
		end
		minetest.set_timeofday(t)
	end,
	drop = {
		max_items=6,
		items = {
			{
				items = {'default:gold_lump'},
				rarity = 99,
			},
			{
				items = {'default:sand'}
			}
		}
	}
})

minetest.register_craft({
	output = "timemeiker:hourglass 1",
	type = "shaped",
	recipe = {
		{"group:glass","group:sand","group:glass"},
		{"","group:glass",""},
		{"group:glass","","group:glass"}
	}
})
